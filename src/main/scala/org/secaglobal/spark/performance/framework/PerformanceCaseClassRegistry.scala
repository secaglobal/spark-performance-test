package org.secaglobal.spark.performance.framework

import java.lang.reflect.Method

import org.secaglobal.spark.performance.framework.annotations.PerformanceCase
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

/**
 * @author Sergey Levandovskiy <levandovskiy.s@gmail.com>
 * @since 2015-06-02
 */
@Component
@Scope
class PerformanceCaseClassRegistry {

  private var cache: Map[Class[_], List[(Method, PerformanceCase)]] = Map()

  def register(clazz: Class[_], methods: List[Method]) = this.synchronized {
    cache = cache + (clazz -> methods.map { method =>
      method -> method.getAnnotation(classOf[PerformanceCase])
    })
  }

  def caseClasses = cache
}


