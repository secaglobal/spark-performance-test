package org.secaglobal.spark.performance.framework.reporters

import java.io.{PrintStream, OutputStream}

import dnl.utils.text.table.TextTable
import org.secaglobal.spark.performance.framework.{SuiteResult, SuccessfulIterationResult}

/**
 * @author Sergey Levandovskiy <levandovskiy.s@gmail.com>
 * @since 2015-06-05
 */
class SimpleTableReporter extends IReporter {
  def process(stream: OutputStream, suites: List[SuiteResult]) {
    suites.map { suite =>
      val ps = new PrintStream(stream)
      ps.println()
      ps.print(suite.title)
      ps.println()

      val iterationColumnsNum: Int = (1 :: suite.cases.map(_.results.size)).max
      val iterationColumns: Array[String] = 1.to(iterationColumnsNum).toArray.map(s"#" + _)
      val columns: Array[String] = Array("Title") ++ iterationColumns

      val values: Array[Array[AnyRef]] = suite.cases.map { caze =>
        Array(caze.title) ++ caze.results.map {
          case SuccessfulIterationResult(duration: Long, _) => "%.01f".format(duration / 1000000000.0)
          case _ => "Err"
        }.padTo(iterationColumnsNum, "N/A").toArray[AnyRef]
      }.toArray

      val table = new TextTable(columns, values)
      table.printTable(ps, 0)
    }
  }
}
