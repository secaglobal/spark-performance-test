package org.secaglobal.spark.performance.cases

import java.nio.file.Paths

import org.apache.spark.SparkContext
import org.apache.spark.sql.{SQLContext, Row}
import org.apache.spark.sql.types._
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * @author Sergey Levandovskiy <levandovskiy.s@gmail.com>
 * @since 2015-06-05
 */
@Component
class AirlineEvent extends Serializable {
  @Autowired
  private val sc: SparkContext = null

  @Autowired
  private val sqlc: SQLContext = null

  lazy val table = {
    val logFile = Paths.get(System.getProperty("spt.workDir", "/tmp"), "airlines.csv").toAbsolutePath.toString

    val logData = sc.textFile(logFile, 2).zipWithIndex().filter(_._2 > 0).map {
      case (line: String, _: Long) => Row.fromSeq(SparkHelper.convertParams(line.replace("\"", "").split(",")))
    }

    val df = sqlc.createDataFrame(logData, SparkHelper.rddSchema)

    df.cache()
    df.count()
    df.registerTempTable("logData")
    sqlc.table("logData")
  }

  lazy val rdd = table.rdd
}

private object SparkHelper {
  def convertParams(raw: Array[String]): Array[Any] = {
    val constructorRef = classOf[AirlineEventEntity].getConstructors()(0)

    constructorRef.getParameterTypes.zipWithIndex.map {
      case (clazz: Class[_], index: Int) if clazz == classOf[Long] => raw(index).toLong
      case (clazz: Class[_], index: Int) if clazz == classOf[Int] => raw(index).toInt
      case (clazz: Class[_], index: Int) if clazz == classOf[Double] => raw(index).toDouble
      case (clazz: Class[_], index: Int) if clazz == classOf[Byte] => raw(index).toByte
      case (clazz: Class[_], index: Int) if clazz == classOf[Float] => raw(index).toFloat
      case (clazz: Class[_], index: Int) if clazz == classOf[Short] => raw(index).toShort
      case (clazz: Class[_], index: Int) => raw(index)
    }
  }

  val rddSchema: StructType = {
    val constructorRef = classOf[AirlineEventEntity].getConstructors()(0)

    val filedsList = constructorRef.getParameters.map { parameter =>
      val sparkType = parameter.getType match {
        case clazz if clazz == classOf[Long] => LongType
        case clazz if clazz == classOf[Int] => IntegerType
        case clazz if clazz == classOf[Double] => DoubleType
        case clazz if clazz == classOf[Byte] => ByteType
        case clazz if clazz == classOf[Float] => FloatType
        case clazz if clazz == classOf[Short] => ShortType
        case clazz => StringType
      }

      StructField(parameter.getName, sparkType)
    }

    StructType(filedsList)
  }
}

sealed class AirlineEventEntity (
                  val ItinID: Long,
                  val MktID: Long,
                  val SeqNum: Byte,
                  val Coupons: Byte,
                  val Year: Short,
                  val Quarter: Byte,
                  val Origin: String,
                  val OriginAptInd: Byte,
                  val OriginCityNum: Int,
                  val OriginCountry: String,
                  val OriginStateFips: Byte,
                  val OriginState: String,
                  val OriginStateName: String,
                  val OriginWac: Byte,
                  val Dest: String,
                  val DestAptInd: Byte,
                  val DestCityNum: Int,
                  val DestCountry: String,
                  val DestStateFips: Byte,
                  val DestState: String,
                  val DestStateName: String,
                  val DestWac: Byte,
                  val Break: String,
                  val CouponType: String,
                  val TkCarrier: String,
                  val OpCarrier: String,
                  val RPCarrier: String,
                  val Passengers: Float,
                  val FareClass: String,
                  val Distance: Float,
                  val DistanceGroup: Byte,
                  val Gateway: Float,
                  val ItinGeoType: Byte,
                  val CouponGeoType: Byte )
