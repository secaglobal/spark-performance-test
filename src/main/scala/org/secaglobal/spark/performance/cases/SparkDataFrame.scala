package org.secaglobal.spark.performance.cases

import org.secaglobal.spark.performance.framework.annotations.{PerformanceCase, PerformanceSuite}
import org.springframework.beans.factory.annotation.Autowired

@PerformanceSuite
class SparkDataFrame {
  @Autowired
  val events: AirlineEvent = null

  @PerformanceCase(value = "Preloading", order = 1)
  def preload = events.table

  @PerformanceCase("DataFrame count")
  def dfLikeCount = events.table.count()

  @PerformanceCase("DataFrame take first")
  def dfLikeTakeFirst = events.table.first()

  @PerformanceCase("DataFrame sort by key + first")
  def dfLikeSortedCountFirst = events.table.sort("arg1").first()

  @PerformanceCase("DataFrame sort by key + count")
  def dfLikeSortedCount = events.table.sort("arg1").count()

  @PerformanceCase("DataFrame group by + first")
  def dfLikeCountByFirst = events.table.groupBy("arg1").count().first()

  @PerformanceCase("DataFrame group by + count")
  def dfLikeCountBy = events.table.groupBy("arg1").count().count()

  @PerformanceCase("DataFrame filter + first")
  def dfLikeFilterFirst = events.table.filter("arg12 = 'New York'").first()

  @PerformanceCase("DataFrame filter + count")
  def dfLikeFilter = events.table.filter("arg12 = 'New York'").count()

  @PerformanceCase("DataFrame filter columns + first")
  def dfLikeColumnsFirst = events.table.select("arg2", "arg22").first()

  @PerformanceCase("DataFrame filter columns + count")
  def dfLikeColumns = events.table.select("arg2", "arg22").count()

  @PerformanceCase("DataFrame distinct + first")
  def dfLikeDistinctFirst = events.table.distinct.first()

  @PerformanceCase("DataFrame distinct + count")
  def dfLikeDistinct = events.table.distinct.count()
}
